import { legacy_createStore } from "redux";
import { reducers } from "../Reducers";

export const store=legacy_createStore(reducers)