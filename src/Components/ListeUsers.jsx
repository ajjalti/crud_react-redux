import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { dalete_user, filter_user } from "../Config/Actions";

function ListeUsers() {
  const dispatch = useDispatch();
  const [nom, setNom] = useState("");
  const [isAffich, setIsAffich] = useState(false);
  const usersTables = useSelector((state) => state.users.usersTables);
  const usersFilter = useSelector((state) => state.users.usersFilter)
  const users = usersFilter.length!==0 ? usersFilter : usersTables;
  function handelFilter(e) {
    e.preventDefault();
    dispatch(filter_user(nom));
  }
  useEffect(() => {
    if (users.length !== 0) setIsAffich(true);
    
  }, [users]);
  return (
    <div className="container mt-5">
      <h1 className="text-center mb-5">
        Bienvenue dans mon application de gestion des utilisateurs avec React
        Redux 🎉 🎉 👋
      </h1>
      {isAffich && (
        <div className="mb-4">
          <form className="d-flex">
            <input
              className="form-control me-sm-2"
              type="search"
              placeholder="Filter par un nom utilisateur !!"
              onChange={(e) => setNom(e.target.value)}
            />
            <button
              className="btn btn-secondary my-2 my-sm-0"
              type="submit"
              onClick={handelFilter}
            >
              Filter
            </button>
          </form>
        </div>
      )}
      <div className="ajoute d-flex justify-content-center mb-4 mt-0">
        <Link to={"/add-user"}>
          <button type="button" className="btn btn-light">
            Ajouter utilisateur
          </button>
        </Link>
      </div>
      {isAffich && (
        <table className="table text-center mb-5">
          <thead>
            <tr>
              <th scope="col">#Id</th>
              <th scope="col">Nom</th>
              <th scope="col">Prénom</th>
              <th scope="col">Gmail</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {users.map((user) => {
              return (
                <tr key={user.id}>
                  <th scope="row">{user.id}</th>
                  <td>{user.nom}</td>
                  <td>{user.prenom}</td>
                  <td>{user.gmail}</td>
                  <td>
                    {/* <UpdateUser/> */}
                    <Link to={`/update-user/${user.id}`}>
                      <button
                        type="button"
                        className="btn btn-outline-info me-3"
                      >
                        Editer
                      </button>
                    </Link>
                    <button
                      type="button"
                      className="btn btn-outline-primary "
                      onClick={() => dispatch(dalete_user(user.id))}
                    >
                      Supprimer
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
    </div>
  );
}

export default ListeUsers;
