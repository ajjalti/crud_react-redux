import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { update_user } from "../Config/Actions";
function UpdateUser() {
  const { id } = useParams();
  const users = useSelector((state) => state.users.usersTables);
  const user = users.find((user) => user.id === parseInt(id));
  const dispatch = useDispatch();
  const [nom, setNom] = useState(user.nom);
  const [prenom, setPrenom] = useState(user.prenom);
  const [gmail, setGmail] = useState(user.gmail);
  const navigate = useNavigate();
  function handelClick() {
    dispatch(update_user({ id,nom, prenom, gmail }));
    navigate("/");
  }

  return (
    <div className="container">
      <h1 className="text-center mt-5">Editer un utilisateur </h1>
      <div className="form-group">
        <label
          className="col-form-label col-form-label-lg mt-4"
          htmlFor="inputLarge"
        >
          Nom :
        </label>
        <input
          className="form-control form-control-lg"
          type="text"
          id="inputLarge"
          value={nom}
          onChange={(e) => setNom(e.target.value)}
        />
        <label
          className="col-form-label col-form-label-lg mt-4"
          htmlFor="inputLarge"
        >
          Prénom :
        </label>
        <input
          className="form-control form-control-lg"
          type="text"
          id="inputLarge"
          value={prenom}
          onChange={(e) => setPrenom(e.target.value)}
        />
        <label className="form-label mt-4">Gmail</label>
        <div className="form-floating mb-3">
          <input
            type="email"
            className="form-control"
            id="floatingInput"
            value={gmail}
            onChange={(e) => setGmail(e.target.value)}
          />
          <label htmlFor="floatingInput">name@example.com</label>
        </div>
        <button
          type="button"
          className="btn btn-primary float-end"
          onClick={handelClick}
        >
          Editer
        </button>
      </div>
    </div>
  );
}

export default UpdateUser;
