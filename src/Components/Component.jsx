import React from 'react'
import { Route, Routes } from 'react-router-dom'
import AddUser from './AddUser'
import Footer from './Footer'
import ListeUsers from './ListeUsers'
import NavBar from './NavBar'
import UpdateUser from './UpdateUser'

function Component() {
  return (
    <div>
      <NavBar />
      <Routes>
        <Route path={"/"} element={<ListeUsers />} />
        <Route path={"/add-user"} element={<AddUser />} />
        <Route path={"/update-user/:id"} element={<UpdateUser />} />
      </Routes>
      <Footer/>
    </div>
  );
}

export default Component